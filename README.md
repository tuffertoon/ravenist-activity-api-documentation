# Ravenist Activity API - Documentation


## Introduction
This is a documentation for [Ravenist](https://www.ravenist.com) [Activity page](https://www.ravenist.com/activity) API. It allows you to get latest sales on Ravenist marketplace in JSON format. The goal of this API is to allow other developers create <s>cool shit.</s> custom push notifications, email notifications, Discord bots, implement Ravenist activity into their own website and many more.

Ravenist aims to be the number one NFT marketplace for people to buy, sell, and create NFTs. Ravenist is backed by Ravencoin.

## Project status
Project is in early development, bugs and reduced functionality are to be expected.

## Data from Ravenist
Activities from Ravenist are scraped every 10 minutes, meaning it can take up to 10 minutes for the transaction to appear in the API response. In case of this project being a success, I am sure we will be able to lower this number.


## Authentication
Currently, there is no authentication required. This may change in the future.


## Rate limiting
Rate limits are 10 requests per 1 minute for each endpoint. Again, these rate limits are subject to change.

## Usage
<b>Base url:</b> `https://api.tuffertoon.io/ravenist_activity`

|        Type                          |       Endpoint       |
|--------------------------------------|----------------------|
| get all                              | `/all`               |
| get latest `'count'`                 | `/latest/'count'`    |
| get all where `'username'` is buyer  | `/buyer/'username'`  |
| get all where `'username'` is seller | `/seller/'username'` |

You can try this API out by simply navigating to one of these endpoints in your web browser or using `curl` in your terminal (e.g. `curl https://api.tuffertoon.io/ravenist_activity/all`).

## Errors
| Code |                        Description                |         Type          |
|------|---------------------------------------------------|-----------------------|
| 200  | OK - everything worked as expected                |                       |
|      | too many activities - the number of requested activities is higher than maximum allowed number (usually 100) | invalid_request_error |
| 404  | not found - the requested resource does not exist | invalid_request_error |
| 429  | too many requests - exceeding the rate limit      | invalid_request_error |


## Contributing
Since the Ravenist Activity API is not currently open-sourced, there is no way to submit pull requests.

If you find any mistake in the API behavior, please create an issue. Also, please report any typo or mistakes in the documentation itself.

I would love to implement any new ideas and features. If you have some, don't hesitate to contact me.


## Authors
Both Ravenist Activity API and it's documentation have been created by <b>Miki_1414</b> from <b>tuffertoon</b> team. \
Check us out over on [our website](https://tuffertoon.io), [twitter](https://www.twitter.com/tuffertoon), [Ravenist](https://www.ravenist.com/accounts/tuffertoon), or join our [Discord](https://discord.gg/nPtWgv8Z).



